# dplayer的使用（研究视频格式支持）

## 项目操作

### 初始化
```
npm install
```
### 启动服务
```
npm run serve
```
### 打包
```
npm run build
```

## 项目介绍

本项目主要是研究学习dplayer，实验dplayer可以播放哪些格式视频。

## 项目结构

主要使用的东西如下：

- public
    - index.html    (需要引入flv.js)
    - static
        - js    (主要放flv.js)
        - video     (主要放flv视频文件)
- src
    - assets
        - video     (视频文件)
    - views
        - useDPlayer.vue    (使用dplayer，实验dplayer播放视频)

## 项目日志

### 2021.02.04
将dplayer学习了一遍，并摸清楚了它支持的格式视频，以及使用方法。

### 2022.12.30
研究对m3u8格式视频的播放支持。

## 外部支持

http://dplayer.js.org/zh/guide.html