import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    redirect: '/useDPlayer'
  },
  {
    path: '/useDPlayer',
    name: 'useDPlayer',
    component: () => import('../views/useDPlayer.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router